# Interview - `react-native` Kanban Board

## Overview

Welcome, and thank you for taking the time to complete this assignment. We are excited to see what you can do!
Candidates are expected to require no more than 4 hours on this assignment.

The repo provided is a non-functional Kanban board app built with `expo`.
The goal of this exercise is to make the Kanban board functional and add some additional features.

## Functionality Requirements

### Phase 1 - The Kanban board

- The board contains 4 stages of tasks in sequence - 'Backlog', 'To Do', 'Ongoing' and 'Done'.

- The 'New Task Name' input should initially be empty. The user can type a task name in this input box and clicking on 'Create' button should add a new task with this task name. This newly created task should be added to the Backlog stage (the first stage). Post this, clear the input field.

- If 'Create' button is clicked with the input being empty, nothing should happen.

- In every individual stage, the tasks are rendered as a list `<ul>` where each task is a single list item `<li>` which displays the name of the task.
- Each task list item has 3 icon buttons at the right -

  1. Back button - This moves the task to the previous stage in sequence, if any. This button is disabled if the task is in the first stage.
  1. Forward button - This moves the task to the next stage in sequence, if any. This button is disabled if the task is in the last stage.
  1. Delete button - This removes the task from the board.

- Each task has 3 properties -
  1. id - A unique identifier for the task. [STRING]
  1. description - description of task. [STRING]
  1. stage - stage of task [NUMBER] (0 represents Backlog stage, 1 represents To Do stage, 2 represents Ongoing stage, 3 represents Done stage).

Notably, the app includes a server running on port `3030` and has the following endpoints:

- `GET /tasks` - Returns all tasks in the board.
- `POST /tasks` - Creates a new task. The request body should be a JSON object with the following properties -
  - `description` - description of task. [STRING]
  - `stage` - stage of task [NUMBER] (0 represents Backlog stage, 1 represents To Do stage, 2 represents Ongoing stage, 3 represents Done stage).
- `PUT /tasks/:id` - Updates a task. The request body should be a JSON object with the following properties -
  - `description` - description of task. [STRING]
  - `stage` - stage of task [NUMBER] (0 represents Backlog stage, 1 represents To Do stage, 2 represents Ongoing stage, 3 represents Done stage).
- `DELETE /tasks/:id` - Deletes a task.

Accordingly, the missing functionality should be developed to interact with the server using fetching solutions
such as `axios`, `fetch`, or `@tanstack/react-query`.

### Phase 2 - The Kanban card page

> The look and feel of this page is not that important; there's not enough "stuff" to make it look good.
> The goal is to demonstrate your ability to navigate between screens, pass data between screens, and handle forms.

- When a task is clicked, the user should be navigated to a new page that displays the details of the task.
- The page should display the task name, description and stage.
- The page should support editing the task name and description by hitting the API.
- The page should have some sort of navigation to go back home.

## Testing Requirements

The application is preloaded with `jest` + `@testing-library/react-native` for unit & functional tests and `detox` for e2e tests.
There are no specific requirements made for test coverage.
A candidate should write tests in a manner and scope that they would _actually_ write them in a real project.
If you think a test is necessary or reasonable, write it. If you don't, don't!
Be prepared to back up your decisions with reasoning.

## Technical Requirements

- All code should be written in TypeScript except for `detox` tests, which should be written in JavaScript.
  For JavaScript files, add `// @ts-check` at the top to get type-checking.
- The react-native app is bootstrapped with `expo` and is preloaded with many common react-native/expo libraries.
  You may use any of the libraries provided. You may also use additional non-UI-related libraries if you wish.
- The app can be developed using any simulator including the web simulator.

If `detox` testing using iOS, you will need to install the following prerequisites:

```sh
brew tap wix/brew
brew install applesimutils
```

To see the list of already installed simulators, run:

```sh
applesimutils --list | jq ".[].name"
```

Then, update the `.detoxrc.json` file to use the simulator you want to use in the `devices` section.

## Getting started

1. Clone the repo
1. Install dependencies with `npm install`
1. `cd ios && pod install && cd ..`
1. Run the server with `npm run server`
1. In another terminal, run the app with `npm start`
   - This application was written by a developer using the `web` simulator without much regard for the look and feel of mobile devices. Accordingly, for best results, use the `web` simulator by pressing `w` in the terminal.
1. Run the Jest tests with `npm test`
1. Run the Detox tests with `npm run detox:ios:debug:build` and `npm run detox:ios:debug:test`
   - Android detox tests are currently not supported using the `debug` build. Try the `release` build instead.

## Submission

Please submit your assignment as a link to a private GitHub or equivalent repo or attach as `.zip` file.
