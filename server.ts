// You shouldn't have to edit this file but you're welcome to if you want to.
import { factory, primaryKey } from "@mswjs/data";
import cors from "cors";
import express, { Request, Response } from "express";

interface Task {
  id: string;
  description: string;
  stage: number;
}

const app = express();

const corsOptions = {
  methods: "GET,PUT,POST,DELETE",
  origin: true,
  optionsSuccessStatus: 200,
};

app.options("*", cors(corsOptions));
app.use(cors(corsOptions));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb", extended: true }));

const projectName = "ID";

const nextId = (() => {
  let id = 0;
  return () => `${projectName}-${++id}`;
})();

const db = factory({
  task: {
    id: primaryKey(() => nextId()),
    description: String,
    stage: Number,
  },
});

db.task.create({
  description: "Simplify data fetching for timesheet view",
  stage: 0,
});
db.task.create({ description: "Upgrade react-native to 0.70.x", stage: 0 });

/**
 * @apiExample {curl} Example usage:
 * curl -X GET http://localhost:3030/tasks
 */
app.get("/tasks", (req: Request, res: Response) => {
  const tasks = db.task
    .getAll()
    .map((t) => ({ id: t.id, description: t.description, stage: t.stage }));

  return res.json(tasks);
});

/**
 * @apiExample {curl} Example usage:
 * curl -X POST http://localhost:3030/tasks -H 'Content-Type: application/json' -d '{ "description": "test", "stage": 0 }'
 */
app.post("/tasks", async (req: Request, res: Response) => {
  const { body } = req;
  console.log(body);
  if (!("description" in body || "stage" in body)) {
    return res.status(400).send("Missing both description and stage");
  }
  const typedBody = body as Task;
  try {
    const task = db.task.create(typedBody);
    return res.json(task);
  } catch (e) {
    return res.status(400).send(e);
  }
});

/**
 * @apiExample {curl} Example usage:
 * curl -X PUT http://localhost:3030/tasks/ID-1 -H 'Content-Type: application/json' -d '{ "description": "test", "stage": 0 }'
 */
app.put("/tasks/:id", async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;
  if (!id || typeof id !== "string") {
    return res.status(400).send("Id invalid");
  }
  const typedBody = body as Pick<Task, "id"> & Partial<Task>;
  const data: Partial<Omit<Task, "id">> = {};
  if ("description" in typedBody && typeof typedBody.description === "string") {
    data.description = typedBody.description;
  }
  if ("stage" in typedBody && typeof typedBody.stage === "number") {
    data.stage = typedBody.stage;
  }
  try {
    const task = db.task.update({
      where: {
        id: {
          equals: id,
        },
      },
      data,
    });
    return res.json(task);
  } catch (e) {
    return res.status(400).send(e as any);
  }
});
/**
 * @apiExample {curl} Example usage:
 * curl -X DELETE http://localhost:3030/tasks/ID-1
 */
app.delete("/tasks/:id", (req: Request, res: Response) => {
  const { id } = req.params;
  if (!id || typeof id !== "string") {
    return res.status(400).send("Missing id");
  }
  try {
    db.task.delete({
      where: {
        id: {
          equals: id,
        },
      },
    });
    return res.sendStatus(200);
  } catch (e) {
    return res.status(400).send(e);
  }
});

const port = process.env.PORT || 3030;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
