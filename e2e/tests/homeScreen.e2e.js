// @ts-check
const { by, device, element, expect } = require('detox');
const { openAppForDebugBuild } = require("./utils/openAppForDebugBuild");

describe("Home screen", () => {
  beforeEach(async () => {
    await device.launchApp({
      newInstance: true,
    });
    await openAppForDebugBuild();
  });

  it('"Create task" button should be visible', async () => {
    await expect(element(by.id("create-task-button"))).toBeVisible();
  });
});
