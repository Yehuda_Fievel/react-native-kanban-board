// @ts-check
const { device } = require("detox");

afterAll(async () => {
  await device.uninstallApp();
});
