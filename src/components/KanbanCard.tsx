import { MaterialIcons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { delTask, putTask, Task } from '../models/task';

export interface KanbanCardProps {
  task: Task;
}

export default function KanbanCard(props: KanbanCardProps) {
  const navigation = useNavigation<any>();
  const queryClient = useQueryClient();

  const useMutateTasks = (mutationFn: (task: object) => Promise<any>) => {
    return useMutation({
      mutationFn,
      onSuccess: (data, variables) => {
        queryClient.resetQueries(['tasks']);
      },
    });
  };

  const { mutate: mutateUpdate } = useMutateTasks(putTask);
  const { mutate: mutateDel } = useMutateTasks(delTask);

  const { task } = props;
  const safeTaskId = task.id.split(' ').join('-');

  const onPressUpdate = (num) => {
    if (task.stage + num > 3 || task.stage + num < 0) {
      return;
    }
    mutateUpdate({ ...task, stage: task.stage + num });
  };

  const onPressDel = () => {
    mutateDel(task);
  };

  const onPressNavigate = () => {
    navigation.navigate('Task', { task });
  };

  return (
    <TouchableOpacity style={styles.container} onPress={onPressNavigate}>
      <View style={styles.textContainer}>
        <Text style={styles.taskIdentifier} testID={`${safeTaskId}-id`}>
          {task.id}
        </Text>
        <Text style={styles.description} testID={`${safeTaskId}-description`}>
          {task.description}
        </Text>
      </View>
      <View style={styles.iconContainer}>
        <View style={styles.iconLeftContainer}>
          <MaterialIcons.Button
            name='chevron-left'
            onPress={() => onPressUpdate(-1)}
            size={16}
            color='black'
            backgroundColor='transparent'
            style={styles.iconButton}
            testID={`${safeTaskId}-back`}
          />
          <MaterialIcons.Button
            name='chevron-right'
            onPress={() => onPressUpdate(1)}
            size={16}
            color='black'
            backgroundColor='transparent'
            style={styles.iconButton}
            testID={`${safeTaskId}-forward`}
          />
        </View>
        <MaterialIcons.Button
          name='delete'
          onPress={onPressDel}
          size={16}
          color='white'
          backgroundColor='#dc3545'
          style={{ ...styles.iconButton, maxWidth: 32 }}
          testID={`${safeTaskId}-delete`}
        />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 16,
    margin: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  iconButton: {
    marginLeft: 2,
    marginRight: 2,
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  iconLeftContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  description: {
    fontSize: 16,
    lineHeight: 24,
    overflow: 'visible',
  },
  taskIdentifier: {
    fontSize: 12,
    fontWeight: 'bold',
    lineHeight: 16,
  },
  textContainer: {},
});
