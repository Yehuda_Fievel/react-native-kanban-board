import { memo } from "react";
import { Task } from "../models/task";
import KanbanCard from "./KanbanCard";

export interface KanbanRowProps {
  task: Task;
}

const KanbanRow = memo(({ task }: KanbanRowProps) => {
  return (
    <KanbanCard task={task} />
  );
});

KanbanRow.displayName = "KanbanRow";

export default KanbanRow;
