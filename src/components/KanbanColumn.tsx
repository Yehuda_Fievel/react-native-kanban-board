import { Ref, useRef } from "react";
import { StyleSheet, Text, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { Task } from "../models/task";
import KanbanRow from "./KanbanRow";

export interface KanbanColumnProps {
  scrollEnabled?: boolean;
  keyExtractor: (item: Task, index: number) => string;
  columnWidth: number;
  columnHeight?: number;
  columnName: string;
  rows: Task[];
}

const KanbanColumn = ({
  keyExtractor,
  scrollEnabled = true,
  columnWidth,
  columnHeight,
  columnName,
  rows,
}: KanbanColumnProps) => {
  const columnRef = useRef<FlatList<any> | undefined>();

  const renderRowItem = ({ item }: { item: Task }) => {
    return <KanbanRow task={item} />;
  };

  const setRef: Ref<FlatList<any>> = (ref) => {
    columnRef.current = ref;
  };

  return (
    <View style={[styles.container, { minWidth: columnWidth }]}>
      <Text style={styles.header}>{columnName.toUpperCase()}</Text>
      <View style={[styles.lane, { minWidth: columnWidth, minHeight: columnHeight }]}>
        <FlatList
          ref={setRef}
          data={rows}
          extraData={[rows, rows.length]}
          renderItem={renderRowItem}
          keyExtractor={keyExtractor}
          nestedScrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEnabled={scrollEnabled}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 4,
    marginHorizontal: 4,
    padding: 4,
    borderColor: "#ccc",
  },
  header: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#444",
  },
  lane: {
    backgroundColor: "#f4f5f7",
    overflow: 'scroll',
    marginTop: 4,
    minHeight: 644,
    maxHeight: 644,
  }
});

export default KanbanColumn;
