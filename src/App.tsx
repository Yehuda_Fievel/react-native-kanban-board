import { DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import HomeScreen from './screens/Home';
import TaskScreen from './screens/Task';

const Stack = createNativeStackNavigator();
const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(255, 45, 85)',
  },
};

const queryClient = new QueryClient();

export default function App() {
  return (
    <NavigationContainer theme={theme}>
      <QueryClientProvider client={queryClient}>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <Stack.Navigator>
            <Stack.Screen
              name='Home'
              component={HomeScreen}
              options={{
                headerStyle: {
                  backgroundColor: '#000',
                },
                headerTitle: 'Kanban board',
                headerTitleStyle: {
                  color: 'green',
                  fontSize: 20,
                },
              }}
            />
            <Stack.Screen name='Task' component={TaskScreen} />
          </Stack.Navigator>
        </GestureHandlerRootView>
      </QueryClientProvider>
    </NavigationContainer>
  );
}
