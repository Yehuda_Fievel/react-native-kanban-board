import { useQuery } from '@tanstack/react-query';
import { StyleSheet, View } from 'react-native';
import KanbanColumn from '../components/KanbanColumn';
import { getTasks, Task } from '../models/task';

export interface KanbanBoardProps {}

const stagesNames = ['Backlog', 'To Do', 'In Progress', 'Done'];

export default function KanbanBoard({}: KanbanBoardProps) {
  // Each task is uniquely identified by its name.
  // Therefore, when you perform any operation on tasks,
  // make sure you pick tasks by names (primary key)
  // instead of any kind of index or any other attribute.

  const { data, error, isError, isLoading } = useQuery(['tasks'], getTasks);
  const stagesTasks: Task[][] = [];

  // Routes:
  // GET /tasks; returns 200 with all tasks (Task[])
  // POST /tasks; returns 200 with the new task (Task) or 400 with an error message
  // PUT /tasks/:id; returns 200 with the new task (Task) or 400 with an error message
  // DELETE /tasks/:id; returns 200 or 400 with an error message
  // Use either `fetch`, `axios`, or `react-query` to perform these operations.

  if (isLoading) {
    return <div>Loading...</div>;
  }
  if (isError) {
    return <div>Error! {error.toString()}</div>;
  }

  for (let i = 0; i < stagesNames.length; ++i) {
    stagesTasks.push([]);
  }
  for (const task of data) {
    const stageId = task.stage;
    stagesTasks[stageId].push(task);
  }

  return (
    <View style={styles.container}>
      <View style={styles.columnsContainer}>
        {stagesTasks.map((tasks, index) => (
          <KanbanColumn
            columnName={stagesNames[index]}
            key={index}
            rows={tasks}
            keyExtractor={(task) => `${task.id}`}
            columnWidth={244}
            columnHeight={644}
          />
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  createTaskContainer: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  columnsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
