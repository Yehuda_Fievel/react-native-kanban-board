import { useMutation, useQueryClient } from '@tanstack/react-query';
import React, { useState } from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';
import { putTask } from '../models/task';

interface Props {
  route: any;
  navigation: any;
}

export default function Task(props: Props) {
  const [text, setText] = useState('');
  const queryClient = useQueryClient();

  const { task } = props.route.params;

  const useMutateTasks = () => {
    return useMutation({
      mutationFn: putTask,
      onSuccess: (data, variables) => {
        queryClient.resetQueries(['tasks']);
      },
    });
  };

  const { mutate } = useMutateTasks();

  const onPress = () => {
    mutate({ ...task, description: text });
  };

  return (
    <>
      <Text>Name: {task.id}</Text>
      <Text>Stage: {task.stage}</Text>
      <Text>Description: {task.description}</Text>
      <TextInput style={styles.textInput} onChangeText={(newText) => setText(newText)} testID='update-task-input' />
      <Button title='Create' onPress={onPress} testID='update-task-button' />
    </>
  );
}

const styles = StyleSheet.create({
    button: {
      maxHeight: 8,
      justifyContent: 'center',
    },
    textInput: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
  });
  