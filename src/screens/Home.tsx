import { useMutation, useQueryClient } from '@tanstack/react-query';
import React, { useState } from 'react';
import { Button, SafeAreaView, StyleSheet, TextInput, View } from 'react-native';
import { postTask, Task } from '../models/task';
import KanbanBoard from '../views/KanbanBoard';

export default function HomeScreen() {
  const useMutateTasks = () => {
    const queryClient = useQueryClient();

    return useMutation({
      mutationFn: postTask,
      onSuccess: (data, variables) => {
        const tasks: Task[] = queryClient.getQueryData(['tasks']);
        tasks.unshift(data);
        queryClient.setQueryData(['tasks'], tasks);
      },
    });
  };

  const [text, setText] = useState('');
  const { isLoading, isError, error, mutate } = useMutateTasks();

  const onPress = () => {
    mutate({ stage: 0, description: text });
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.createTaskContainer}>
        <TextInput style={styles.textInput} onChangeText={(newText) => setText(newText)} testID='create-task-input' />
        <Button title='Create' onPress={onPress} testID='create-task-button' />
      </SafeAreaView>
      <KanbanBoard />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'center',
  },
  createTaskContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    maxHeight: 8,
    justifyContent: 'center',
  },
  textInput: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
