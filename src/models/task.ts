import axios from 'axios';

export interface Task {
  id: string;
  description: string;
  stage: number;
}

export async function getTasks() {
  const { data } = await axios.get('http://localhost:3030/tasks');
  return data;
}

export async function postTask(tasks) {
  const { data } = await axios.post('http://localhost:3030/tasks', tasks);
  return data;
}

export async function putTask(task) {
  const { data } = await axios.put(`http://localhost:3030/tasks/${task.id}`, task);
  return data;
}

export async function delTask(task) {
  const { data } = await axios.delete(`http://localhost:3030/tasks/${task.id}`);
  return data;
}
